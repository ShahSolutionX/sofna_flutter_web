import 'package:flutter/material.dart';
import 'package:web_flutter_test/services/api_service.dart';
import 'package:web_flutter_test/widgets/appbar.dart';
import 'package:web_flutter_test/widgets/feature_row.dart';

class HomeScreen extends StatefulWidget {
  static const String name = 'homepage';
  const HomeScreen({Key? key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  String imageUrl = '';

  @override
  void initState() {
    super.initState();
    fetchBannerImage();
  }

  Future<void> fetchBannerImage() async {
    final url = await RPC().getImages('get_images', 'banner');
    if (url != null) {
      setState(() {
        imageUrl = url;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(65.0),
          child: screenWidth > 600
              ? buildDestopAppBar(context)
              : buildMobileAppBar(context),
        ),
        primary: false,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.width / 1.618,
                margin: const EdgeInsets.symmetric(vertical: 24),
                decoration: imageUrl.isNotEmpty
                    ? BoxDecoration(
                        color: Colors.amber,
                        image: DecorationImage(
                          image: NetworkImage(imageUrl),
                          fit: BoxFit.cover,
                        ),
                      )
                    : null,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height,
                child: Center(
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 60,
                      ),
                      const Text(
                        'PERBEZAAN PAKEJ PERCUTIAN SOFNA TRAVEL\nBERBANDING YANG LAIN',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 3,
                            child: buildColumn('NO TOURIST TRAP',
                                '‘Tourist Trap’ ialah tempat paksa beli yang orang lokal tak pergi. Anda akan dibawa ke bangunan/dewan mendengar ceramah jualan jed, herba, ginseng dll. Buang masa sebab bukan ‘real attraction. ARBA tidak bawa anda tempat ini.'),
                          ),
                          const SizedBox(
                            width: 50,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 3,
                            child: buildColumn('STANDARD & FULL EXPERIENCE',
                                'Pilih pakej standard untuk pakej percutian yang lengkap atau anda boleh pilih pakej full experience untuk pakej yang lebih unik dari segi hotel dan aktiviti sepanjang percutian.'),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 3,
                            child: buildColumn(
                              'SOLAT & MAKAN HALAL TERJAGA',
                              'Itinerari disusun supaya anda dapat solat pada waktunya di tempat yang sesuai. Kami juga pastikan anda makan makanan halal yang ikut selera & citarasa orang Malaysia.',
                            ),
                          ),
                          const SizedBox(width: 50),
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 3,
                            child: buildColumn(
                              'HOTEL DI LOKASI STRATEGIK',
                              'Anda akan bermalam di kawasan tumpuan untuk nikmati ‘nightlife’ di luar negara. Pakej lain yang lebih murah akan beri anda hotel di luar bandar (jauh) untuk ‘cut cost’.',
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 3,
                            child: buildColumn(
                              'URUSAN VISA 100%',
                              'Anda cuma perlu beri detail dan dokumen, kami yang akan uruskan VISA anda mengikut destinasi pakej percutian yang anda ambil.',
                            ),
                          ),
                          const SizedBox(width: 50),
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 3,
                            child: buildColumn(
                              'DIPERCAYAI SEJAK 2015',
                              'ARBA Travel berdaftar dengan Kementerian Pelancongan & telah bawa lebih 30,000 rakyat Malaysia melancong sejak 2015. Kami terima rating positif di Facebook (900+) & Google (400+).',
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
