import 'package:flutter/material.dart';

class DestinationsScreen extends StatelessWidget {
  const DestinationsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Destinations'),
      ),
      body: const Center(
        child: Text('Explore our Destinations'),
      ),
    );
  }
}
