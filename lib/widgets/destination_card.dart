import 'package:flutter/material.dart';

class DestinationCard extends StatelessWidget {
  const DestinationCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: const Text('Destination Name'),
        subtitle: const Text('Description and details about the destination'),
        onTap: () {
          Navigator.pushNamed(context, '/destinations');
        },
      ),
    );
  }
}
