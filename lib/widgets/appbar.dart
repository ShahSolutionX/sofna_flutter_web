import 'package:flutter/material.dart';

AppBar buildMobileAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.red,
    title: const Text(
      'SOFNA',
      style: TextStyle(
        fontSize: 20.0,
        letterSpacing: 5,
        fontWeight: FontWeight.bold,
      ),
    ),
    actions: [
      MediaQuery.of(context).size.width > 600
          ? Row(
              children: [
                const Spacer(),
                TextButton(
                  onPressed: () {
                    // Handle button tap action
                  },
                  child: const Text(
                    'Luar Negara',
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(width: 20),
                TextButton(
                  onPressed: () {
                    // Handle button tap action
                  },
                  child: const Text(
                    'Dalam Negara',
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.white,
                    ),
                  ),
                ),
                // Add more buttons as needed
              ],
            )
          : PopupMenuButton(
              itemBuilder: (context) => [
                const PopupMenuItem(
                  child: Text('Luar Negara'),
                  value: 'Luar Negara',
                ),
                const PopupMenuItem(
                  child: Text('Dalam Negara'),
                  value: 'Dalam Negara',
                ),
                // Add more menu items as needed
              ],
              onSelected: (value) {
                // Handle menu item selection
              },
            ),
    ],
  );
}

AppBar buildDestopAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.red,
    title: const Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: 50,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'SOFNA',
              style: TextStyle(
                  fontSize: 20.0,
                  letterSpacing: 5,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              'TRAVEL',
              style: TextStyle(
                fontSize: 10.0,
                letterSpacing: 5,
              ),
            ),
          ],
        ),
        Spacer(),
        Text(
          'Luar Negara',
          style: TextStyle(
            fontSize: 16.0,
          ),
        ),
        SizedBox(
          width: 50,
        ),
        Text(
          'Dalam Negara',
          style: TextStyle(
            fontSize: 16.0,
          ),
        ),
        SizedBox(
          width: 50,
        ),
        Text(
          'Honeymoon',
          style: TextStyle(
            fontSize: 16.0,
          ),
        ),
        SizedBox(
          width: 50,
        ),
        Text(
          'Company Trip',
          style: TextStyle(
            fontSize: 16.0,
          ),
        ),
        SizedBox(
          width: 50,
        ),
        Text(
          'Tentang SOFNA',
          style: TextStyle(
            fontSize: 16.0,
          ),
        ),
        SizedBox(
          width: 50,
        ),
        Text(
          'Utama',
          style: TextStyle(
            fontSize: 16.0,
          ),
        ),
      ],
    ),
  );
}
