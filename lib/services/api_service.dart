import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:web_flutter_test/services/common.dart';

class RPC {
  final String baseUrl = 'https://x8ki-letl-twmt.n7.xano.io/api:UoTTXcui';

  Future<String?> getImages(String apiName, String parameter) async {
    try {
      final response =
          await http.get(Uri.parse('$baseUrl/$apiName?name=banner'));
      if (response.statusCode == 200) {
        final List<dynamic> data = json.decode(response.body);
        if (data.isNotEmpty) {
          String? imageUrl = data[0]['url'];

          return imageUrl;
        }
      } else {
        Common.debugPrint(
            'Failed to fetch data', 'Status code: ${response.statusCode}');
      }
    } catch (e) {
      Common.debugPrint('Exception', e.toString());
    }
    return null;
  }
}
