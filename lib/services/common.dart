import 'package:flutter/foundation.dart';

enum LogLevel { debug, info, warning, error }

class Common {
  static void debugPrint(String title, String message) {
    if (kDebugMode) {
      String logType = _getLogType(LogLevel.debug);
      String timestamp = DateTime.now().toIso8601String();
      print('[$logType] $timestamp - $title : $message');
    }
  }

  static String _getLogType(LogLevel level) {
    switch (level) {
      case LogLevel.debug:
        return 'DEBUG';
      case LogLevel.info:
        return 'INFO';
      case LogLevel.warning:
        return 'WARNING';
      case LogLevel.error:
        return 'ERROR';
    }
  }
}
