class Destination {
  final String name;
  final String description;

  Destination({
    required this.name,
    required this.description,
  });
}
